<?php

namespace api\modules\v1\models;

use OAuth2\Storage\UserCredentialsInterface;
use filsh\yii2\oauth2server\models\OauthAccessTokens as AccessToken;

class Usuario extends \common\models\User implements UserCredentialsInterface
{
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()->joinWith('accessTokens', false)
            ->andWhere(['access_token' => $token])
            ->one();
    }
    
    /**
     * @inheritdoc
     */
    public function checkUserCredentials($username, $password)
    {
        $user = static::findByUsername($username);
        if (empty($user)) {
            return false;
        }
        return $user->validatePassword($password);
    }

    /**
     * @inheritdoc
     */
    public function getUserDetails($username = null)
    {
        $user = $username
            ? static::findByUsername($username)
            : $this;
        return ['user_id' => $user->id];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getAccessTokens()
    {
        return $this->hasMany(AccessToken::className(), [
            'user_id' => 'id',
        ]);
    }
}
