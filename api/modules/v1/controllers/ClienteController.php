<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Cliente;
use Yii;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use filsh\yii2\oauth2server\filters\ErrorToExceptionFilter;
use filsh\yii2\oauth2server\filters\auth\CompositeAuth;
/**
 * CLIENTE Controller API
 *
 */
class ClienteController extends ActiveController
{ 
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    ['class' => HttpBearerAuth::className()],
                    [
                        'class' => QueryParamAuth::className(),
                        // Importante, parametro get a usar.
                        'tokenParam' => 'accessToken',
                    ],
                ],
            ],
            'exceptionFilter' => [
                'class' => ErrorToExceptionFilter::className(),
            ],
        ]);
    }

    public $modelClass = 'api\modules\v1\models\Cliente';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        $actions['view']['findModel'] = [$this, 'view'];
       // $actions['create']['findModel'] = [$this, 'create'];
        $actions['delete']['findModel'] = [$this, 'delete'];
        $actions['update']['findModel'] = [$this, 'update'];

        return $actions;
    }

    public function view($id, $action)
    {
        $cliente = Cliente::find()->where(['id_cliente'=>$id])->all();

            if($cliente == []){
                return ["message"=>"Cliente no registrado en el Sistema, Verifica el número."];
            }else{
                return $cliente;
            }
    }
}
?>
