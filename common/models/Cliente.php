<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property integer $id_cliente
 * @property string $nombre
 * @property integer $iduser
 * @property string $apellido_materno
 * @property string $apellido_paterno
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property string $estado_civil
 * @property string $email
 * @property integer $tipo_identificacion
 * @property string $id_identificacion
 * @property double $promedio_gastos_mensual
 * @property string $observaciones
 * @property string $documentacion
 * @property string $rfc
 * @property string $curp
 * @property string $clabe_banco
 * @property integer $id_banco
 * @property string $facebook_id
 * @property string $facebook_token
 * @property string $entidad_nacimiento
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 * @property integer $status
 * @property integer $historico
 * @property integer $servicio
 * @property string $username
 * @property integer $active
 * @property string $generador
 * @property string $homorfc
 * @property string $homocurp
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'iduser', 'apellido_materno', 'apellido_paterno', 'fecha_nacimiento', 'clabe_banco', 'id_banco', 'auth_key', 'password_hash', 'fecha_alta', 'fecha_actualizacion'], 'required'],
            [['iduser', 'tipo_identificacion', 'id_banco', 'status', 'historico', 'servicio', 'active'], 'integer'],
            [['fecha_nacimiento', 'fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['promedio_gastos_mensual'], 'number'],
            [['nombre', 'id_identificacion'], 'string', 'max' => 128],
            [['apellido_materno', 'apellido_paterno', 'rfc', 'curp'], 'string', 'max' => 64],
            [['sexo'], 'string', 'max' => 2],
            [['estado_civil', 'clabe_banco', 'generador'], 'string', 'max' => 5],
            [['email', 'facebook_token'], 'string', 'max' => 256],
            [['observaciones'], 'string', 'max' => 1024],
            [['documentacion'], 'string', 'max' => 5120],
            [['facebook_id'], 'string', 'max' => 20],
            [['entidad_nacimiento', 'username', 'homorfc', 'homocurp'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cliente' => 'Id Cliente',
            'nombre' => 'Nombre',
            'iduser' => 'Iduser',
            'apellido_materno' => 'Apellido Materno',
            'apellido_paterno' => 'Apellido Paterno',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'sexo' => 'Sexo',
            'estado_civil' => 'Estado Civil',
            'email' => 'Email',
            'tipo_identificacion' => 'Tipo Identificacion',
            'id_identificacion' => 'Id Identificacion',
            'promedio_gastos_mensual' => 'Promedio Gastos Mensual',
            'observaciones' => 'Observaciones',
            'documentacion' => 'Documentacion',
            'rfc' => 'Rfc',
            'curp' => 'Curp',
            'clabe_banco' => 'Clabe Banco',
            'id_banco' => 'Id Banco',
            'facebook_id' => 'Facebook ID',
            'facebook_token' => 'Facebook Token',
            'entidad_nacimiento' => 'Entidad Nacimiento',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
            'status' => 'Status',
            'historico' => 'Historico',
            'servicio' => 'Servicio',
            'username' => 'Username',
            'active' => 'Active',
            'generador' => 'Generador',
            'homorfc' => 'Homorfc',
            'homocurp' => 'Homocurp',
        ];
    }
}
